# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2018 Zsin Skri <zsin@posteo.net>

# HOW TO USE:
#   begin your bash script with:
#   ```
#   #!/usr/bin/env bash
#   . "${XDG_CONFIG_HOME:-/.config}"/scripts/script-consts.sh "<program name>" || exit 1
#   load-ext <extension>...
#   ```

set -euo pipefail
IFS=$'\n\t'

# args
script_path="$(readlink -f "$(type -p "$0")")"
program_name="$1"

# XDG-BASE-DIR
: "${XDG_CACHE_HOME:=$HOME/.cache}"
: "${XDG_CONFIG_HOME:=$HOME/.config}"
: "${XDG_DATA_HOME:=$HOME/.local/share}"

script_dir="${script_path%/*}"

function load-ext {
    while ! [ "$#" = 0 ]; do
        . "$script_dir"/extensions/"$1".sh || {
            rc="$?"
            echo "$program_name: failed to$RED load extension$NOCO $1"
            return "$rc"
        }
        shift
    done
}
