function die {
    local code="$1"
    local msg="$2"

    printf "%s: %s\n" "$program_name" "$msg" >&2
    exit "$code"
}
