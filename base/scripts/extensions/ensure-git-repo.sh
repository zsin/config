function ensure-git-repo {
    # clone and update a git repository
    local url="$1"
    local dir="$2"
    local ref="${3:-master}"

    if [ -d "$dir" ]; then
        pushd "$dir" >/dev/null
        if ! [ "$(git remote get-url origin || echo none)" = "$url" ]; then
            echo "ensure-git-repo: directory ($dir) already exists" >&2
            return 1
        fi
        echo "updating $dir"
        git pull origin
    else
        mkdir -p "$(dirname "$dir")"
        echo "cloning $url into $dir"
        git clone --recurse-submodules "$url" "$dir"
        pushd "$dir" >/dev/null
    fi
    if [ "$(git rev-parse --abbrev-ref HEAD)" != "$ref" ]; then
        echo "checkout $ref in $dir"
        git checkout "$ref"
    fi
    popd >/dev/null
}
