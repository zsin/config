if [ -z "${local:-}" ]; then
    load-ext tmp cleanup
    local="$(tmpd)"
    add-cleanup 'rm -rf "$local"'
fi
