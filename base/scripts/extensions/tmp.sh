function tmpd {
    mktemp --tmpdir -d "${1:-$program_name}".XXXXXXXXXX"${2:-}"
}

function tmpf {
    mktemp --tmpdir "${1:-$program_name}".XXXXXXXXXX"${2:-}"
}
