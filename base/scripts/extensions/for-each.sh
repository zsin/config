function for-each {
    while read -r line; do
        "$@" "$line"
    done
}
