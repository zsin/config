function confirm {
    local prompt="$1"'?' # [y/n]
    local default="${2:-}"

    case "$default" in
        [yY]*) prompt="$prompt"' [Y/n] ';;
        [nN]*) prompt="$prompt"' [y/N] ';;
        "") prompt="$prompt"' [y/n] ';;
        *)
            echo "$program_name: confirm: invalid default: '$default'"
            exit 1;;
    esac

    local answer=''
    while true; do
        case "$answer" in
            [yY]*)
                return 0;;
            [nN]*)
                return 1;;
            *)
                read -n 1 -p "$prompt" answer
                echo # newline after read
                if [ -z "$answer" ]; then
                    answer="$default"
                fi
                ;;
        esac
    done
}
