_cleanup=()

function cleanup {
    for cmd in "${_cleanup[@]}"; do
        eval "$cmd"
    done
}

function add-cleanup {
    _cleanup["${#_cleanup[@]}"]="$@"
}

trap cleanup EXIT
