_getopt_T_ret="0"
getopt -T || {
    _getopt_T_ret="$?"
}
if ! [ "$_getopt_T_ret" = 4 ]; then
    echo "Could not find util-linux getopt! (try pure-getopt?)" >&2
    exit 4
fi

function parse-args {
    local short="$1"
    local long="$2"
    set -- "$(getopt -o "$short" -l "$long" --name "$program_name" -- "$@")"
    while [ "$1" != -- ]; do
        parse-arg
        shift
    done
}
