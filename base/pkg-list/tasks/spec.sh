#-- base
# pre-installed
/ base-system
# system management
/ git
/ dots
# basic utils
/ neovim
/ cmatrix
