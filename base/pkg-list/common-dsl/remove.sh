load-ext data tmp
mkdir -p "$data"/remove

function remove {
    # expect instructions on stdin
    local pkg_name="${1:-$task}"

    local path="$data"/remove/"$pkg_name"
    mkdir -p "$(dirname "$path")"
    cat > "$path"
    reg dont-remove "$pkg_name"
    require-actor remove
}

function register-removes {
    local dir="$1"; shift
    # pkg names as args
    # instructions from stdin (pkg-name will be in $pkg)

    local instructions="$(tmpf)"
    cat > "$instructions"

    for pkg in "$@"; do
        cat <(echo "pkg='$pkg'") "$instructions" | \
            remove "$dir"/"$pkg"
    done

    rm "$instructions"
}
