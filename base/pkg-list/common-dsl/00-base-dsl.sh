lists="$local"/lists
mkdir -p "$lists"
function get-list-path {
    case "$#" in
        1)
            local path="$lists"/"$1";;
        2)
            local path="$lists"/"$1"/"$2"
            mkdir -p "$(dirname "$path")";;
    esac
    touch -a "$path"
    printf "%s\n" "$path"
}

function reg {
    # register a line to a list
    local list="$1"; shift
    printf -v line "\t%s" "$@"
    local line="${line:1}"
    echo "$line" >> "$(get-list-path "$list")"
}

function require-actor {
    command rm "$(get-list-path skip-actors "$1")"
}

function skip-actor {
    : $(get-list-path skip-actors "$1")
}
