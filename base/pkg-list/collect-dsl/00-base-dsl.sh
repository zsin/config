load-ext data

function / {
    collect-task "$@"
}

function /prefix {
    while read -r line; do
        / "$1"/"$line"
    done
}

function list-cmd {
    # list command
    #  Create a command adding arguments to `list`.
    local list="$1"
    eval "
        function $list {
            reg '$list' \"\$@\"
        }
    "
}

function opt-list-cmd {
    # optional list command
    #  Create a command adding arguments to `list`.
    #  `actor` will only be run when that command
    #  was called at least once.
    #  `actor` must use the remove-actor.
    local list="$1"
    local actor="${2:-$list}"

    : $(get-list-path skip-actors "$actor")
    eval "
        function $list {
            require-actor '$actor'
            reg $list \"\$@\"
        }
    "
}

function dep-list-cmd {
    # dependency list command
    #  Create a command adding arguments to `list`.
    #  When called at least one time, `dependency`
    #  will be collected.
    local list="$1"
    local dependency="${2:-$list}"

    eval "
        function $list {
            / '$dependency'
            echo '$dependency' >> '$data'/pkg-list/dep-list-cmd/installed-deps
            reg $list \"\$@\"
        }
    "
}
# Preserve `dependency` once after this becomes not called.
# `dependency` is most likely also needed to remove the now
# unwanted packages.
mkdir -p "$data"/pkg-list/dep-list-cmd
if [ -e "$data"/pkg-list/dep-list-cmd/installed-deps ]; then
    while read -r dep "$data"/pkg-list/dep-list-cmd/installed-deps; do
        / "$dep"
    done
    truncate -s 0 "$data"/pkg-list/dep-list-cmd/installed-deps
fi

function opt-dep-list-cmd {
    # optional dependency list command
    #  Create a command adding arguments to `list`.
    #  When called at least one time, `dependency`
    #  will be collected, and only then will `actor`
    #  be run.
    #  `actor` must use the remove-actor.
    local list="$1"
    local actor="${2:-$list}"
    local dependency="${3:-$actor}"

    skip-actor "$actor"
    eval "
        function $list {
            / '$dependency'
            require-actor '$actor'
            reg $list \"\$@\"
        }
    "
}
