function lib-git {
    / git
    require-actor local-lib
    local name="$1"
    local url="$2"
    local ref="${3:-master}"
    reg lib-git "$name" "$url" "$ref"
}

opt-dep-list-cmd lib-zip local-lib unzip
opt-dep-list-cmd lib-tar local-lib tar
opt-list-cmd lib-file local-lib
