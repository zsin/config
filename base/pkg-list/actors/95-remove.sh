load-ext data confirm

while read -ru 4 pkg_name; do
    local remove_script="$data"/remove/"$pkg_name"
    announce "$pkg_name"
    if confirm "remove $pkg_name"; then
        . "$remove_script" && {
            rm "$remove_script"
        } || {
            echo "${RED}remove $pkg_name$NOCO failed!"
        }
    else
        echo "${YELLOW}skipping$NOCO remove $pkg_name."
    fi
done 4< <(comm -23 \
               <(find "$data/remove" -mindepth 1 -type f -printf '%P\n' | sort | uniq) \
               <(sort "$(get-list-path dont-remove)" | uniq))
