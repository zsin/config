mkdir -p "$XDG_DATA_HOME"/fonts
rebuild_cache=false

while read -ru 4 path; do
    name="$(basename "$path")"
    announce "$name"

    path="$XDG_LIB_HOME"/"$path"
    dest="$XDG_DATA_HOME"/fonts/"$name"
    if [ "$(readlink "$dest")" != "$path" ]; then
        ln -sfT "$path" "$dest"
        rebuild_cache=true
    fi
done 4< <(list font | unique)

if [ "$rebuild_cache" = true ]; then
    announce rebuild font cache
    fc-cache
fi
