pushd "$XDG_LIB_HOME" >/dev/null

announce copy-file
while read -ru 4 src dest mode owner group; do
    sudo install -C -m "$mode" -o "$owner" -g "$group" \
            "$src" "$dest"
done 4< <(list copy-file | unique)

popd >/dev/null
