load-ext ensure-git-repo

announce git
while read -ru 4 name url ref; do
    dir="$XDG_LIB_HOME"/"$name"
    ensure-git-repo "$url" "$dir" "$ref"

    remove lib-git/"$name" <<EOF
rm -rf '$dir'
EOF
done 4< <(list lib-git | unique)


announce zip
while read -ru 4 name url; do
    dir="$XDG_LIB_HOME"/"$name"
    if [ ! -e "$dir" ]; then
        echo "installing $YELLOW$name$NOCO to $dir"
        zip="$(tmpf pl-lib-zip-"$name" .zip)"
        wget -O "$zip" "$url"
        unzip -d "$dir" "$zip"
        rm "$zip"
    fi

    remove lib-zip/"$name" <<EOF
rm -rf '$dir'
EOF
done 4< <(list lib-zip | unique)

announce tar
while read -ru 4 name url; do
    dir="$XDG_LIB_HOME"/"$name"
    if [ ! -e "$dir" ]; then
        echo "installing $YELLOW$name$NOCO to $dir"
        tar="$(tmpf pl-lib-tar-"$name" .tar)"
        wget -O "$tar" "$url"
        mkdir -p "$dir"
        gtar --directory "$dir" --extract --file "$tar"
        rm "$tar"
    fi

    remove lib-zip/"$name" <<EOF
rm -rf '$dir'
EOF
done 4< <(list lib-tar | unique)

announce file
while read -ru 4 name url; do
    dest="$XDG_LIB_HOME"/"$name"
    if [ ! -e "$dest" ]; then
        echo "downloading $YELLOW$name$NOCO"
        wget -O "$dest" "$url"
    fi
    remove lib-zip/"$name" <<EOF
rm '$dest'
EOF
done 4< <(list lib-file | unique)
