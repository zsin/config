function list {
    local path="$(get-list-path "$@")"
    [ -e "$path" ] &&
        cat "$path"
}

function announce {
    echo "    $actor ${YELLOW}$@${NOCO}"
}

function if-args {
    local cmd="$1"; shift
    if [ "$#" != 0 ]; then
        echo '$' "$cmd $@"
        eval "$cmd"' "$@"'
    fi
}

function unique {
    awk '!seen[$0]++'
}

function ask {
    # expect list of objects in file "$1"
    while read -ru 4 line; do
        if confirm "$line" >/dev/tty; then
            printf '%s\n' "$line"
        fi
    done 4< "$1"
}

function unknown-pkg {
    # expect installed packages in "${installed[@]}"
    # optionally pass known in $1 (as file)
    local known="${1:-/dev/null}"
    unknown <(cat "$known" <(list pkg) <(list keep) <(list rm))
}

function unknown {
    # expect installed packages in "${installed[@]}"
    # pass known in $1 (as file)
    comm -23 \
            <(cat-array "${installed[@]}") \
            <(sort "$1" | uniq)
}

function remove-pkg {
    # expect installed packages in "${installed[@]}"
    comm -12 \
            <(cat-array "${installed[@]}") \
            <(list rm)
}

function install-pkg {
    # expect installed packages in "${installed[@]}"
    list pkg | only-missing
}

function only-missing {
    # expect installed packages in "${installed[@]}"
    # packages to filter on stdin
    comm -23 \
            <(sort | uniq) \
            <(cat-array "${installed[@]}")
}
