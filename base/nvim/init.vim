set nocompatible

" keybindings
imap fd <Esc>

" enable filetype detection (plugin and indentation)
filetype plugin indent on

" enable syntax highlighting
syntax on
" show relative line numbers
set number relativenumber

" indentation
" use indentation level from previous line
set autoindent
" use indentation type from previous line
set copyindent

" backspace moves over EOL and indents
set backspace=indent,eol,start

" when substituting, split window to show changes
set inccommand=split

" lowercase -> ignore case, capitalised -> use case
set ignorecase smartcase
" highlight search terms
set hlsearch
" start searching directly
set incsearch

" read settings from file
set modeline
