#-- base
# thanks to https://wiki.archlinux.org/index.php/XDG_Base_Directory_support !!!

# android
export ANDROID_SDK_ROOT="$XDG_LIB_HOME"/android-sdk
export ANDROID_SDK_HOME="$ANDROID_SDK_ROOT"
# bash
export HISTFILE="$XDG_DATA_HOME"/bash/history
# ccache
export CCACHE_DIR="$XDG_CACHE_HOME"/ccache
# GnuPG
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
# GTK
export GTK_RC_FILES="$XDG_CONFIG_HOME"/gtk-1.0/gtkrc
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
# ICE
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
# ipython
export IPYTHONDIR="$XDG_DATA_HOME"/ipython
# java
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
# less
export LESSHISTFILE="$XDG_DATA_HOME"/less/history
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
# spacemacs
export SPACEMACSDIR="$XDG_CONFIG_HOME"/spacemacs
# sqlite3
alias sqlite3="sqlite3 -init '$XDG_CONFIG_HOME/sqlite3/sqliterc'"
# MPlayer
export MPLAYER_HOME="$XDG_CONFIG_HOME"/mplayer
# GNU parallel
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel
# GNU readline
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
# ruby gems
export GEM_HOME="$XDG_DATA_HOME"/ruby-gem
export GEM_SPEC_CACHE="$XDG_CACHE_HOME"/ruby-gem
# terminfo
export TERMINFO="$XDG_DATA_HOME"/terminfo
export TERMINFO_DIRS="$TERMINFO":/usr/share/terminfo
# tig
export TIGRC_USER="$XDG_CONFIG_HOME"/tig/tigrc
# wget
export WGETRC="$XDG_CONFIG_HOME/wgetrc"
alias wget="wget --hsts-file='$XDG_CACHE_HOME/wget-hsts'"
# wine
export WINEPREFIX="$XDG_DATA_HOME"/wineprefixes/default
# xcompose
export XCOMPOSEFILE="$XDG_CONFIG_HOME"/xcompose/compose
export XCOMPOSECACHE="$XDG_CACHE_HOME"/xcompose
