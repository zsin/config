. ~/.config/bash/xdg_base_dir_env.sh
. "$XDG_CONFIG_HOME"/bash/xdg_base_dir_compatibility.sh
. "$XDG_CONFIG_HOME"/bash/path.sh

# assume X11 env (for potential use see scripts/wait-for-x)
export DISPLAY=:0
export XAUTHORITY=~/.Xauthority

# default programs
export EDITOR=nvim
export VISUAL="$EDITOR"
export PAGER=less

