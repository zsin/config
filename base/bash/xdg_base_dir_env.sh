# set XDG Base Directory default values
export XDG_CONFIG_HOME=~/.config
export XDG_CACHE_HOME=~/.cache
export XDG_DATA_HOME=~/.local/share
# set XDG Base Directory extensions default values
export XDG_BIN_HOME=~/.local/bin
export XDG_LIB_HOME=~/.local/lib
