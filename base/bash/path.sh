#-- base (1/2)
# init MANPATH
MANPATH="$(manpath -q)"
# add scripts and ~/.local/bin to PATH
PATH="$XDG_CONFIG_HOME"/scripts:~/.local/bin:"$PATH"

!!@@

#-- base (2/2)
export PATH
export MANPATH
