# source global definitions
[ -f "${PREFIX:-}"/etc/bashrc ] && . "${PREFIX:-}"/etc/bashrc

# set XDG_*_HOME
# this is only needed when bash_profile was not yet executed
. ~/.config/bash/xdg_base_dir_env.sh

# if not running interactively, stop here
[[ $- != *i* ]] && return

# bash options
shopt -s dotglob  # include dotted files in glob expansions

# set prompt
. "$XDG_CONFIG_HOME"/bash/prompt.sh

# define aliases
. "$XDG_CONFIG_HOME"/bash/aliases.sh

# source functions
if [ -d "$XDG_CONFIG_HOME"/bash/functions ]; then
    for func in "$XDG_CONFIG_HOME"/bash/functions/*; do
        . "$func"
    done
fi

# source completions
for f in "${PREFIX:-}"{/usr,}/share/bash-completion/bash_completion; do
    [ -f "$f" ] &&
        . "$f" &&
        break
done
if [ -d "$XDG_CONFIG_HOME"/bash/completions ]; then
    for comp in "$XDG_CONFIG_HOME"/bash/completions/*; do
        . "$comp"
    done
fi

# setup history
#   also see:
#   - xdg_base_dir_compatibility.sh (HISTFILE)
export HISTCONTROL=ignoredups:erasedups
export HISTSIZE=-1
shopt -s histappend
__on_command_entered () {
    # write history to file
    builtin history -a

    # reset color after prompt
    echo -en "\e[m"
}
trap __on_command_entered DEBUG
