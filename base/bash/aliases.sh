#-- base
# perform alias substition after sudo
alias sudo='sudo '

# versions
alias python='python3'
alias ipython='ipython3'

# color
alias diff='diff --color=always'
alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias tree='tree -C'
alias less='less -R'

# human readable sizes
alias du='du -h'
alias df='df -h'

# shortcuts
alias pl='pkg-list'
