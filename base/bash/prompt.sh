# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2018 Zsin Skri <zsin@posteo.net>

## BASH PROMPT
# dark green ">"
__PS1_prefix="\[\e[38;5;22m\]>"
# abbreviate $PWD
__PS1_abrv_path() {
    # create abbreviated $PWD ($HOME/.local/etc/base -> ~/.l/e/base)
    local path="$PWD"

    # replace $HOME/ with ~/
    if [ "$path" = "$HOME" ] || [[ "$path" == "$HOME"/* ]]; then
        path="~${path#$HOME}"
    fi

    # shorten directory names
    while [[ $path =~ (.*/)(\..|[^\.])[^/]+/(.*) ]]; do
        path="${BASH_REMATCH[1]}${BASH_REMATCH[2]}/${BASH_REMATCH[3]}"
    done

    printf "%s" "$path"
}
# gray italic path
__PS1_path="\[\e[38;5;8m\e[3m\]\$(__PS1_abrv_path)\[\e[m\]"
# red exit status if $? != 0
PROMPT_COMMAND='__EXIT_CODE="$?"'
__PS1_exit_status() {
    [ "$__EXIT_CODE" != 0 ] &&
        echo -en "\x01\e[38;5;1m\e[1m\x02${__EXIT_CODE}\x01\e[m\x02 "
}
# username (if not $MYUSER), # for root and set color for postfix
__PS1_user_iden () {
    # check root
    [ "$EUID" = 0 ] &&
        echo -en "\x01\e[38;5;9m\x02#"
    [ "$USER" != "$MYUSER" ] &&
        printf '%s' "$USER"
}
__PS1_user="\[\e[38;5;2m\]\$(__PS1_user_iden)"
__PS1_postfix="> \[\e[m\e[38;5;2m\]"

# set default $MYUSER
MYUSER="${MYUSER:-zsin}"

# set prompt
PS1="${__PS1_prefix} ${__PS1_path} \$(__PS1_exit_status)${__PS1_user}${__PS1_postfix}"

# dots default append point !!@@
