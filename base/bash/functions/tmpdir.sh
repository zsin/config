function tmpdir {
    pushd "$(mktemp -d)" >/dev/null
    printf '%s\n' "$PWD"
}
