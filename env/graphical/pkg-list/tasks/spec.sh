#-- env/graphical
# fonts
/ font/adobe/source-code-pro
/ font/adobe/source-sans-pro
/ font/adobe/source-serif-pro
/ font/adobe/source-han-sans
/ font/adobe/source-han-serif
/ font/mozilla/fira-mono
/ font/mozilla/fira-sans
/ font/google/roboto
/ font/google/roboto-slab
/ font/google/roboto-mono
# password manager
/ keepassxc
