list-cmd kwin-script
list-cmd plasmoid

function __kpackage_git_cmd {
    local type="$1"
    local name="$type-$2"
    local url="$3"
    local installed_name="${4:-$name}"

    lib-git "$name" "$url"
    "$type" "$XDG_LIB_HOME"/"$name" "$installed_name"
}

function kwin-script-git {
    __kpackage_git_cmd kwin-script "$@"
}

function plasmoid-git {
    __kpackage_git_cmd plasmoid "$@"
}
