function kpackage {
    local type="$1"
    local src="$2"
    local dest="$XDG_DATA_HOME/$3"

    mkdir -p "$XDG_DATA_HOME"/kservices5

    if [ -d "$dest" ]; then
        action='--upgrade'
    else
        action='--install'
    fi
    kpackagetool5 --type "$type" "$action" "$src"
}

function kpackage-rm {
    local type="$1"
    local name="$2"

    echo "removing $RED$name$NOCO:"
    kpackagetool5 --type "$type" -r "$name"
}

function kpackage-unknown {
    # expect known on stdin
    local type="$1"

    installed=("$(kpackagetool5 --type "$type" --list | tail -n +2 | sort | uniq)")
    unknown /dev/stdin | \
        for-each kpackage-rm "$type"
}

announce kwin-scripts
while read -ru 4 src installed_name; do
    kpackage KWin/Script "$src" kwin/scripts/"$installed_name"
    cp \
        "$src"/metadata.desktop \
        "$XDG_DATA_HOME"/kservices5/"$installed_name".desktop
    printf '%s\n' "$installed_name" >&9
done 4< <(list kwin-script | unique) 9> >(kpackage-unknown KWin/Script)


announce plasmoids
while read -ru 4 src installed_name; do
    kpackage Plasma/Applet "$src" plasma/plasmoids/"$installed_name"
    printf '%s\n' "$installed_name" >&9
done 4< <(list plasmoid | unique) 9> >(kpackage-unknown Plasma/Applet)

