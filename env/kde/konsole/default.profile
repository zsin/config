[Appearance]
ColorScheme=Adapta
Font=Monospace,10,-1,5,50,0,0,0,0,0,Regular

[General]
Name=Default
Parent=FALLBACK/

[Interaction Options]
OpenLinksByDirectClickEnabled=false
UnderlineFilesEnabled=true

[Scrolling]
HistoryMode=2
