# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2018 Zsin Skri <zsin@posteo.net>
#
# Setup this configuration repo on this machine.
# This script should be sourced by a bash shell!
#
# The following will be done:
#   1. Move this repo to ~/.local/etc
#   2. Clone dots (https://github.com/EvanPurkhiser/dots) into ~/.local/lib/dots.
#   3. Symlink dots into ~/.local/bin.
#   4. Temporarily add ~/.local/bin to PATH.
#   5. Set dots group (prompts).
#   6. Install dotfiles.
#   7. Source installed bash config.
#   8. Update dotfiles git url.
#   9. Give advice.
#

__config_install_main() {
    local dots_url='https://github.com/EvanPurkhiser/dots.git'
    local config_repo=~/.local/etc
    local dots_repo=~/.local/lib/dots
    local bin_dir=~/.local/bin

    # ANSI colours
    local RED="$(tput setaf 1)"
    local GREEN="$(tput setaf 2)"
    local YELLOW="$(tput setaf 3)"
    local NOCO="$(tput sgr0)"

    # get repo dir
    local cloned_repo="$(cd "$(dirname "$BASH_SOURCE")"; pwd)"

    local answer


    echo 'install configuration repo from "'"$(basename "$cloned_repo")"'"'

    # create directories
    mkdir -p "$(dirname "$config_repo")" "$(dirname "$dots_repo")" "$bin_dir"

    # 1. Move this repo to ~/.local/etc
    if [ "$cloned_repo" != "$config_repo" ]; then
        echo "move to $config_repo"
        if [ -e "$config_repo" ]; then
            echo "$config_repo""${RED}already exists${NOCO}, aborting"
            return 1
        fi
        mv -i "$cloned_repo" "$config_repo"
    else
        echo "already at $config_repo"
    fi

    # 2. Clone dots into ~/.local/lib/dots
    echo "clone dots into $dots_repo"
    if [ -e "$dots_repo" ]; then
        # dir already exists
        # check if it is a dots repo
        if [ "$(cd "$dots_repo"; git remote get-url origin)" == "$dots_url" ]; then
            read -p "${YELLOW}dots already cloned${NOCO} to ${dots_repo}. Pull master? [Y/n] " answer
            case "$answer" in
                [nN]*) echo "${RED}aborting${NOCO}"; return 1;;
                *) (
                    set -e
                    pushd "$dots_repo" >/dev/null
                    git checkout master
                    git pull
                    popd >/dev/null
                )
            esac
        else
            echo "${dots_repo} ${RED}already exists${NOCO}, aborting!"
            return 1
        fi
    else
        git clone "$dots_url" "$dots_repo"
    fi

    # 3. Symlink dots into ~/.local/bin
    ln -sf "$dots_repo"/bin/dots "$bin_dir"/dots

    # 4. prepend ~/.local/bin to PATH
    echo "prepend $bin_dir to PATH"
    export PATH="$bin_dir":"$PATH"

    # 5. Set dots groups to hostname (which contains ~/.config/dots/config-groups).
    echo "known ${GREEN}groups${NOCO}:"
    dots groups known
    # change dir to $repo_dest/host to allow tap completion of hosts
    pushd "$config_repo"/host >/dev/null
    local host
    read -ei "$(hostname -s)" -p "Choose ${GREEN}host${NOCO}: " host
    popd >/dev/null
    # create host directory if needed
    mkdir -p "$config_repo"/host/"$host"
    # set groups
    dots groups set base "host/$host"

    # 6. Install dotfiles (after confirm).
    # get dots groups from repo
    while [ -n "$("$dots_repo"/bin/dots diff --shortstat dots/config-groups)" ]; do
        dots install dots/config-groups
        echo "got ${GREEN}groups${NOCO}:"
        dots groups current
    done
    # print information
    echo "${GREEN}installing${NOCO} files:"; dots files
    echo "from these groups:${GREEN}"; dots groups current; echo "$NOCO"
    read -p "Please ${YELLOW}confirm${NOCO} the changes that will be made (press ENTER to view). "
    dots diff --color=always | less -r
    answer=''
    read -p 'Is everything OK? [Y/n] ' answer
    case "$answer" in
        [Nn]*) echo 'aborting'; return 1;;
        *) echo 'running "dots install"'; dots install && echo "${GREEN}INSTALLED!${NOCO}";;
    esac

    # 7. Source installed bash config.
    . ~/.bash_profile || echo "${RED}bash_profile$NOCO failed!"
    . ~/.bashrc || echo "${RED}bashrc$NOCO failed!"

    # 8. Update dotfiles git url.
    echo "setting dotfiles ${GREEN}git url${NOCO}"
    local origin
    read -ei "https://gitlab.com/zsin/config.git" -p "origin url: " origin
    pushd "$config_repo" >/dev/null
    git remote set-url origin "$origin"
    popd >/dev/null

    # 9. Give advice.
    echo -e "\n ${YELLOW}ADVICE${NOCO}:"
    cat <<EOF
  - generate public/private keys with \`\$ gen-key <use> [id] [host]\`
    - e.g. generate ssh keys with \`\$ gen-key ssh zsin\`
  - reboot to globally source ~/.bash_profile
  - update dotfiles with \`\$ dots-in\`
  - update system configuration (including packages) with \`\$ pkg-list\`
EOF

    echo -e "\n${GREEN}DONE!${NOCO}"
}
__config_install_main
unset __config_install_main
