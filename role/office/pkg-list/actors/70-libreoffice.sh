while read -ru 4 pkg; do
    announce "$pkg"
    if ! unopkg list "$pkg" 2>/dev/null >/dev/null; then
        echo "installing..."
        unopkg add "$XDG_LIB_HOME"/"$pkg" && echo done
    fi
    remove libreoffice/"$pkg" <<EOF
unopkg remove '$pkg'
EOF
done 4< <(list libreoffice | unique)
