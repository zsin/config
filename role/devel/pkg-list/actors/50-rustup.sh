announce update self
rustup self update

announce toolchains
toolchains=()
while read -ru 4 toolchain; do
    remove rust-toolchain/"$toolchain" <<EOF
rustup toolchain uninstall '$toolchain'
EOF
    if [ "$toolchain" = nightly ]; then
        if rustup toolchain list | grep 'nightly' >/dev/null; then
            echo 'skiping nighly update (use `$ rustup update`)'
            continue
        fi
    fi
    toolchains+=("$toolchain")
done 4< <(list rust-toolchain | unique)
if-args "rustup toolchain install" "${toolchains[@]}" | grep -v '^[[:blank:]]*$'

announce components
if-args "rustup component add" $(list rust-component | unique)

register-removes rust-component $(list rust-component) <<EOF
rustup component remove "\$pkg"
EOF
