function missing {
    while read -r cmd; do
        if ! type "$cmd" >/dev/null; then
            echo "$cmd"
        fi
    done < <(sort | uniq)
}

announce install
if-args "cargo install" $(list cargo | missing)

announce install-nightly
if-args "cargo +nightly install" $(list cargo-nightly | missing)

register-removes cargo $(list cargo) $(list cargo-nightly) <<EOF
cargo uninstall "\$pkg"
EOF
