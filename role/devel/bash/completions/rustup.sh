if type rustup >/dev/null; then
    # rustup
    eval "$(rustup completions bash)"
    # cargo (installed through rustup)
    . "$(rustc --print sysroot)"/etc/bash_completion.d/cargo
fi
