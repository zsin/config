#-- role/devel
# cargo bin
PATH="$XDG_DATA_HOME"/cargo/bin:"$PATH"
if type rustup >/dev/null; then
    __rustup_man_path="$RUSTUP_HOME/toolchains/$(rustup show active-toolchain)/share/man"
    MANPATH="$__rustup_man_path:$MANPATH"
fi

