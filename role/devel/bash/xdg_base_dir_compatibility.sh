#-- role/devel
# gradle
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
# rust
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
export CARGO_HOME="$XDG_DATA_HOME"/cargo
