lib-git star-ruler-2 https://github.com/BlindMindStudios/StarRuler2-Source.git

/ cmake
/prefix devel <<EOF
zlib
glew
freetype
bzip2
openal
libvorbis
libogg
libXrandr
libcurl
libXi
libGLU
libpng
EOF
