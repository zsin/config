# source this at the beginning of an .install script
# it will set the XDG BASE DIR env vars (and maybe later more)

# load $XDG_*_HOME
. "$DOTS_SOURCE"/base/bash/xdg_base_dir_env.sh
# ensure $XDG_*_HOME exists
"$DOTS_SOURCE"/base/bash/xdg_base_dir_env.sh.install

function create_link {
    # replace $link with a link to $dest
    local link="$1"
    local dest="$2"

    mkdir -p "$dest"
    if [ "$(readlink "$link")" != "$dest" ]; then
        if [ -e "$link" ]; then
            rmdir "$dest" || {
                echo "ERORR: both $link and $dest exist!"
                exit 1
            }
            mv "$link" "$dest"
        fi
        ln -sfT "$dest" "$link"
    fi
}
