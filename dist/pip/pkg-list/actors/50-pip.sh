pkgs=($(list pip | unique))

pip3 install --user -U "${pkgs[@]}"

for pkg in "${pkgs[@]}"; do
    remove pip/"$pkg" <<EOF
pip3 uninstall '$pkg'
EOF
done
