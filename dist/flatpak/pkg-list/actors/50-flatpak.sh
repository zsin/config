announce repos
while read -ru 4 repo url; do
    flatpak remote-add --if-not-exists "$repo" "$url"
    remove flatpak-repo/"$repo" <<EOF
flatpak remote-delete '$repo'
EOF
done 4< <(list flatpak-repo | unique)

announce update
flatpak update

announce flatpaks
while read -ru 4 repo pkg; do
    if [ "$repo" != "$(flatpak info -o "$pkg" 2>/dev/null)" ]; then
        flatpak install "$repo" "$pkg"
    fi
    remove flatpak/"$pkg" <<EOF
flatpak uninstall '$pkg'
EOF
done 4< <(list flatpak | unique)
