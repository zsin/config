opt-dep-list-cmd flatpak-repo flatpak

function flatpak {
    / flatpak
    / flatpak-repo/"$1"
    require-actor flatpak
    reg flatpak "$@"
}
