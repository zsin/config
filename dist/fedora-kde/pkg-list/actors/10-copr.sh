installed=("$(dnf copr list --enabled | sort)")

announce remove
if-args 'sudo dnf copr remove' \
        $(ask <(unknown <(list copr)))

announce enable
if-args 'sudo dnf copr enable' \
        $(ask <(list copr | unique | only-missing))
