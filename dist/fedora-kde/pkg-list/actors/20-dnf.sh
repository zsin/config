installed=("$(dnf --cacheonly repoquery --userinstalled -q --qf '%{name}' | sort | uniq)")

announce remove
if-args "sudo dnf remove" $(remove-pkg)

announce unknown
if-args "sudo dnf remove" $(unknown-pkg <(list no-weak))

announce update
sudo dnf distro-sync

announce no-weak
if-args "sudo dnf install --setopt='install_weak_deps=false'" \
        $(list no-weak | only-missing)

announce install
if-args "sudo dnf install" \
        $(install-pkg)
if-args "sudo dnf mark install" \
        $(install-pkg)

announce cleanup
sudo dnf autoremove
