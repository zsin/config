# install while collecting -> will be available for other tasks
if ! type rustup >/dev/null; then
    echo "Please install ${YELLOW}rustup${NOCO}:"
    echo "curl https://sh.rustup.rs -sSf > rustup-init.sh && less rustup-init.sh"
    echo "sh rustup-init.sh && rm rustup-init.sh"
    return 1
fi
rust-toolchain stable
