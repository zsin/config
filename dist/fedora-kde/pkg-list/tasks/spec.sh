#-- dist/fedora-kde
# themes
/prefix theme <<EOF
icon/papirus
arc
adapta
breeze
EOF
# additional software
/ python3-dnf-plugin-system-upgrade
/ plasma-discover-flatpak
    / xdg-desktop-portal-kde
/ fedora-workstation-repositories
/ gstreamer1-plugin-openh264
/ mozilla-openh264
# kcm
/ plymouth-kcm
/ sddm-kcm
/ kcm_systemd
# system info
/ redhat-lsb
