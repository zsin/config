lib-tar teensy-loader https://www.pjrc.com/teensy/teensy_linux64.tar.gz
lib-file teensy-loader-udev.rules https://www.pjrc.com/teensy/49-teensy.rules
copy-file teensy-loader-udev.rules /etc/udev/rules.d/49-teensy.rules 0664 root root
